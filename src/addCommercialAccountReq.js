import {PostalAddress} from 'postal-object-model';

/**
 * @class {AddCommercialAccountReq}
 */
export default class AddCommercialAccountReq {

    _name:string;

    _address:PostalAddress;

    _phoneNumber:string;

    _customerSegmentId:number;

    _customerBrandId:number;

    /**
     *
     * @param {string} name
     * @param {PostalAddress} address
     * @param {string} phoneNumber
     * @param {number|null} [customerSegmentId]
     * @param {number|null} [customerBrandId]
     */
    constructor(name:string,
                address:PostalAddress,
                phoneNumber:string,
                customerSegmentId:number = null,
                customerBrandId:number = null) {

        if (!name) {
            throw new TypeError('name required');
        }
        this._name = name;

        if (!address) {
            throw new TypeError('address required');
        }
        this._address = address;

        if (!phoneNumber) {
            throw new TypeError('phoneNumber required');
        }
        this._phoneNumber = phoneNumber;

        if (!customerSegmentId && !customerBrandId) {
            throw new TypeError('customerSegmentId or customerBrandId required');
        }
        if (customerSegmentId && customerBrandId) {
            throw new TypeError('customerSegmentId and customerBrandId cannot both be specified');
        }
        this._customerSegmentId = customerSegmentId;
        this._customerBrandId = customerBrandId;
    }


    /**
     * @returns {string}
     */
    get name():string {
        return this._name;
    }

    /**
     * @returns {PostalAddress}
     */
    get address():PostalAddress {
        return this._address;
    }

    /**
     * @returns {string}
     */
    get phoneNumber():string {
        return this._phoneNumber;
    }

    /**
     * @returns {number}
     */
    get customerSegmentId():number {
        return this._customerSegmentId;
    }

    /**
     * @returns {number}
     */
    get customerBrandId():number {
        return this._customerBrandId;
    }

    toJSON() {
        return {
            name: this._name,
            address: this._address.toJSON(),
            phoneNumber: this._phoneNumber,
            customerSegmentId: this._customerSegmentId,
            customerBrandId: this._customerBrandId
        };
    }

}
