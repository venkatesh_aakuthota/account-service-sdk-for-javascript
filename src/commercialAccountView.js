import {PostalAddress} from 'postal-object-model';

/**
 * The most detailed view of a account
 * @class CommercialAccountView
 */
export default class CommercialAccountView {

    _name:string;

    _address:PostalAddress;

    _id:string;

    /**
     * @param {string} name
     * @param {PostalAddress} address
     * @param {string} id
     */
    constructor(name:string,
                address:PostalAddress,
                id:string) {

        if (!name) {
            throw new TypeError('name required');
        }
        this._name = name;

        if (!address) {
            throw new TypeError('address required');
        }
        this._address = address;

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;

    }

    /**
     * @returns {string}
     */
    get name():string {
        return this._name;
    }

    /**
     * @returns {PostalAddress}
     */
    get address():PostalAddress {
        return this._address;
    }

    /**
     * @returns {string}
     */
    get id():string {
        return this._id;
    }

}
