import AccountServiceSdkConfig from '../../src/accountServiceSdkConfig';

export default {
    accountServiceSdkConfig: new AccountServiceSdkConfig(
        'https://account-service-dev.precorconnect.com'
    ),
    identityServiceJwtSigningKey: 'nbho9k9vcv8r48xGQs4woyN8BJ6q9X1efj295KXfS9A9yHJSRm0oU21j3ickrScQ',
    existingPartnerAccountAssociation: {
        partnerAccountId: '001K000001GMV0zIAH',
        partialName: '80231c4eef224bd19a1cfddfbe6f253b'
    },
    existingCustomerSegmentId: 1,
    existingCustomerBrandId: 1
}