import AddCommercialAccountReq from '../../src/addCommercialAccountReq';
import jwt from 'jwt-simple';
import config from './config';
import dummy from '../dummy';

export default {
    constructValidAddCommercialAccountRequest,
    constructValidPartnerRepOAuth2AccessToken
}

function constructValidAddCommercialAccountRequest():AddCommercialAccountReq {

    return new AddCommercialAccountReq(
        dummy.accountName,
        dummy.postalAddress,
        dummy.phoneNumber,
        config.existingCustomerSegmentId
    );
}

function constructValidPartnerRepOAuth2AccessToken():string {

    const tenMinutesInMilliseconds = 10000 * 60;

    const jwtPayload = {
        "type": "partnerRep",
        "exp": Date.now() + tenMinutesInMilliseconds,
        "aud": dummy.url,
        "iss": dummy.url,
        "given_name": dummy.firstName,
        "family_name": dummy.lastName,
        "email": dummy.emailAddress,
        "sub": `${dummy.partnerRepId}`,
        "account_id": config.existingPartnerAccountAssociation.partnerAccountId,
        "sap_vendor_number": dummy.sapVendorNumber
    };

    return jwt.encode(jwtPayload, config.identityServiceJwtSigningKey);
}