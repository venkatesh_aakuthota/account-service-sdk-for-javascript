import AddCommercialAccountReq from '../../src/addCommercialAccountReq';
import dummy from '../dummy';

/*
tests
 */
describe('AddCommercialAccountReq class', () => {
    describe('constructor', () => {
        it('throws if name is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddCommercialAccountReq(
                        null,
                        dummy.postalAddress,
                        dummy.phoneNumber,
                        dummy.customerSegmentId
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'name required');

        });
        it('sets name', () => {
            /*
             arrange
             */
            const expectedName = dummy.accountName;

            /*
             act
             */
            const objectUnderTest =
                new AddCommercialAccountReq(
                    expectedName,
                    dummy.postalAddress,
                    dummy.phoneNumber,
                    dummy.customerSegmentId
                );

            /*
             assert
             */
            const actualName =
                objectUnderTest.name;

            expect(actualName).toEqual(expectedName);

        });
        it('throws if address is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddCommercialAccountReq(
                        dummy.accountName,
                        null,
                        dummy.phoneNumber,
                        dummy.customerSegmentId
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'address required');

        });
        it('sets address', () => {
            /*
             arrange
             */
            const expectedAddress = dummy.postalAddress;

            /*
             act
             */
            const objectUnderTest =
                new AddCommercialAccountReq(
                    dummy.accountName,
                    expectedAddress,
                    dummy.phoneNumber,
                    dummy.customerSegmentId
                );

            /*
             assert
             */
            const actualAddress =
                objectUnderTest.address;

            expect(actualAddress).toEqual(expectedAddress);

        });

        it('throws if phoneNumber is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddCommercialAccountReq(
                        dummy.accountName,
                        dummy.postalAddress,
                        null,
                        dummy.customerSegmentId
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'phoneNumber required');

        });
        it('sets phoneNumber', () => {
            /*
             arrange
             */
            const expectedPhoneNumber = dummy.phoneNumber;

            /*
             act
             */
            const objectUnderTest =
                new AddCommercialAccountReq(
                    dummy.accountName,
                    dummy.postalAddress,
                    expectedPhoneNumber,
                    dummy.customerSegmentId
                );

            /*
             assert
             */
            const actualPhoneNumber =
                objectUnderTest.phoneNumber;

            expect(actualPhoneNumber).toEqual(expectedPhoneNumber);

        });

        it('throws if customerSegmentId & customerBrandId are both null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddCommercialAccountReq(
                        dummy.accountName,
                        dummy.postalAddress,
                        dummy.phoneNumber,
                        null,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(
                TypeError,
                'customerSegmentId or customerBrandId required'
            );

        });
        it('throws if customerSegmentId & customerBrandId both specified', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddCommercialAccountReq(
                        dummy.accountName,
                        dummy.postalAddress,
                        dummy.phoneNumber,
                        dummy.customerSegmentId,
                        dummy.customerBrandId
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(
                TypeError,
                'customerSegmentId and customerBrandId cannot both be specified'
            );

        });
        it('sets customerSegmentId', () => {
            /*
             arrange
             */
            const expectedCustomerSegmentId = dummy.customerSegmentId;

            /*
             act
             */
            const objectUnderTest =
                new AddCommercialAccountReq(
                    dummy.accountName,
                    dummy.postalAddress,
                    dummy.phoneNumber,
                    expectedCustomerSegmentId
                );

            /*
             assert
             */
            const actualCustomerSegmentId =
                objectUnderTest.customerSegmentId;

            expect(actualCustomerSegmentId).toEqual(expectedCustomerSegmentId);

        });
        it('sets customerBrandId', () => {
            /*
             arrange
             */
            const expectedCustomerBrandId = dummy.customerBrandId;

            /*
             act
             */
            const objectUnderTest =
                new AddCommercialAccountReq(
                    dummy.accountName,
                    dummy.postalAddress,
                    dummy.phoneNumber,
                    null,
                    expectedCustomerBrandId
                );

            /*
             assert
             */
            const actualCustomerBrandId =
                objectUnderTest.customerBrandId;

            expect(actualCustomerBrandId).toEqual(expectedCustomerBrandId);

        });
    });
    describe('toJSON method', () => {
        it('returns expected object', () => {
            /*
             arrange
             */
            const objectUnderTest =
                new AddCommercialAccountReq(
                    dummy.accountName,
                    dummy.postalAddress,
                    dummy.phoneNumber,
                    dummy.customerSegmentId
                );

            const expectedObject =
            {
                name: objectUnderTest.name,
                address: objectUnderTest.address.toJSON(),
                phoneNumber: objectUnderTest.phoneNumber,
                customerSegmentId: objectUnderTest.customerSegmentId,
                customerBrandId: null
            };

            /*
             act
             */
            const actualObject =
                objectUnderTest.toJSON();

            /*
             assert
             */
            expect(actualObject).toEqual(expectedObject);

        })
    });
});
