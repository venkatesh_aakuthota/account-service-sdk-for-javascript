import CommercialAccountView from '../../src/commercialAccountView';
import dummy from '../dummy';

/*
test methods
 */
describe('CommercialAccountView class', () => {
    describe('constructor', () => {
        it('throws if name is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new CommercialAccountView(null, dummy.postalAddress, dummy.accountId);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'name required');
        });

        it('sets name', () => {
            /*
             arrange
             */
            const expectedName = dummy.accountName;

            /*
             act
             */
            const objectUnderTest =
                new CommercialAccountView(expectedName, dummy.postalAddress, dummy.accountId);

            /*
             assert
             */
            const actualName = objectUnderTest.name;
            expect(actualName).toEqual(expectedName);

        });
        it('throws if address is null', () => {
            /*
             arrange
             */
            const constructor = () => new CommercialAccountView(dummy.accountName, null, dummy.accountId);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'address required');
        });

        it('sets address', () => {
            /*
             arrange
             */
            const expectedAddress = dummy.postalAddress;

            /*
             act
             */
            const objectUnderTest =
                new CommercialAccountView(expectedAddress, dummy.postalAddress, dummy.accountId);

            /*
             assert
             */
            const actualAddress = objectUnderTest.address;
            expect(actualAddress).toEqual(expectedAddress);

        });
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor = () =>
                new CommercialAccountView(dummy.accountName, dummy.postalAddress, null);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = dummy.accountId;

            /*
             act
             */
            const objectUnderTest =
                new CommercialAccountView(dummy.accountName, dummy.postalAddress, expectedId);

            /*
             assert
             */
            const actualId = objectUnderTest.id;
            expect(actualId).toEqual(expectedId);

        });
    })
});