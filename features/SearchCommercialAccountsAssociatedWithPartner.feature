Feature: Search Commercial Accounts Associated With Partner
  Searches the commercial accounts associated with the provided partner account id by
  partial name match

  Scenario: Success
    Given partnerAccountId is the id of an account with partnerAccountAssociations in the account-service
    And partialName partially matches the names of accounts in partnerAccountAssociations
    And I provide a searchCommercialAccountsAssociatedWithPartnerReq consisting of:
      | attribute        | value            |
      | partnerAccountId | partnerAccountId |
      | partialName      | partialName      |
    And I provide an accessToken identifying me as a partner rep associated with accountId
    When I execute searchCommercialAccountsAssociatedWithPartner
    Then the matching accounts are returned